
body {
  margin: 0px;
  font: 13px/20px Verdana;
  /*color: white;*/
  text-decoration: none;
  background-color: MediumPurple;
}

main {
  width: 960px;
  margin: 0px auto;
  margin-top: 200px;
  padding: 10px;
  border-color: Indigo;
  border-width: 3px;
  border-style: solid;
  background-color: SlateBlue;
}

header {
  opacity: 0.9;
  width: 960px;
  border-right: 300px solid MediumPurple;
  border-bottom: 30px solid MediumPurple;
  height: 130px;
  background-color: MediumPurple;
  position: fixed; /*POSICION FIJA DEL MENU HEADER AUNQUE SE HAGA SCROLL*/
  top: 0px; /*distancia en px hasta la parte superior de su box contenedor*/
  left: 0px;
  right: 0px;
  z-index: 2; /*aparece por encima segun su numero, mayor nro mas prioridad y aparece encima*/
}

header div {
  width: 960px;
  margin: 0px auto;
}

header nav {
  margin-left: 550px;
  width: 500px;
  text-align: center;
}

header nav li {
  display: inline-block; /*para que aparezcan al lado*/
  width: 180px;
  position: relative; /*items que se ven en el menu encabezado con posicion relativa, sin posicion absoluta*/
}

header nav li ul {
  position: absolute; /*cada una de las razas que van a aparecer solo cuando presione razas*/
  display: none; /*para que no aparezca la sublista por defecto*/
  left: 0px;
  right: 0px;
  background-color: #24063c;
}

header nav li:hover ul {
  display: block; /*devuelve la propiedad block al list-item de las razas al pasar el mouse por encima*/
}

header nav li a {
  display: inline-block;
  color: white;
  width: 100%;
  padding: 7px 2px;
}

header nav li a:hover {
  width: 100%;
  background-color: #6f488e;
}

header nav li ul li {
  display: block; /*pone display tipo block*/
  padding: 1px 2px; /*1px arriba y abajo - 4px de costado*/
  width: 90px;
}

header nav li ul li a {
  margin-left: 5px;
  width: 145px;
}

header nav li ul li a:hover {
  width: 180px;
  background-color: #6f488e;
}

header ul {
  margin: 0px;
  padding: 0px;
  list-style-type: none;
}

footer {
  width: 960px;
  background-color: SlateBlue;
  margin: 0px auto;
}

footer ul {
  list-style-type: none;
  display: inline-block;
  margin-top: 20px;
}

#primer {
  color: Indigo;
}

 /*EL CLASS LO LLAMO CON PUNTO*/
.texto_de_imagenes {
  color: grey;
  background-color: Indigo;
  margin: 0px auto;
}

a {
  text-decoration: none;
  color: Indigo;
  font-weight: bold;
}

a:hover {
  color: DarkCyan;
}

a:active {
  color: blue;
}

h1 {
  width: 500px;
  font-family: 'Metal Mania', cursive;
  font-weight: normal; /*SACA LA NEGRITA*/
  text-align: center;
  height: 50px;
  font-size: 40px;
  color: Indigo;
  line-height: 40px;
  letter-spacing: 4px; /*SEPARACION ENTRE CARACTERES*/
  float: left; /*PARA QUE EL TITULO h1 FLOTE A LA IZQUIERDA Y SE ACOMODE EL MENU A LA DERECHA*/
  margin-left: 100px;
  text-shadow: 5px 5px 5px black; /*sombra a letras*/
}

h1 span {
  display: block;
  color: SlateBlue;
  font: 30px;
}

h2 {
  font-family: "Trebuchet Ms";
  font-weight: bold;
  font-size: 30px;
  text-indent: 50px;
  color: Indigo;
}

h3 {
  color: Indigo;
  /*font-family: "Trebuchet Ms";
  font-weight: bold;
  font-size: 25px;
  font-variant: small-caps; /*PEQUE�AS MAYUSCULAS RESTANTES*/
  font: bold italic small-caps 25px/37px "Trebuchet Ms";
  /*text-transform: uppercase; /*MAYUSCULAS*/
  text-transform: capitalize; /*PRIMER LETRA MAYUSCULA*/
  text-indent: 50px;
  text-decoration: underline; /*SUBRAYADO*/
}

p {
  font-family: Verdana;
  line-height: 30px; /*INTERLINEADO*/
  font-weight: bold; /*NEGRITA*/
  text-indent: 50px; /*SANGRIA*/
}

#fotos {
  text-align: center;
  list-style-type: none;
}

nav ul {
  list-style-type: none;
}

main section aside ul {
  list-style-type: none;
}

#fm {
  float: right;
}

#redes li {
  display: inline-block; /*para que ponga los iconos al lado*/
}

blockquote {
  text-align: justify;
  column-count: 3;
  column-rule: 1px solid black;
  column-gap: 40px;
}

article p {
  /*word-break: break-all; para que corte las palabras al llegar al final del parrafo*/
  hyphens: auto; /*para que ponga el guion del corte de palabras*/
}

td {
  padding: 20px 65px 20px 65px; /*arriba derecha abajo izquierda*/
}

#casos {
  width: 100%;
}

#casos caption {
  color: silver;
  font-size: 20px;
  font-family: "Trebuchet Ms";
  background-color: black;
  padding: 10px;
}

#casos th {
  color: white;
  background-color: #191970;
}
